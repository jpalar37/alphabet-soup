package AlphabetSoup;

/**
 * @author Jeremy Palardy
 *
 * This class is for coordinate objects. it's simply just a x and y coordingate and a toString method.
 */
public class Coordinate {
    public final int x;
    public final int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        if (x < 0 || y < 0) {
            return "NONE";
        }
        return x + ":" + y;
    }
}
