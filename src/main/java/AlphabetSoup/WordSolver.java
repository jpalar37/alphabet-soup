/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlphabetSoup;

import javax.sound.sampled.SourceDataLine;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Jeremy Palardy
 *
 * This class is the main engine of the project. This is where the solution is found.
 * The bounds are searched on the word grid first. As each letter is found, the search
 * index increases. The process keeps repeating until the search index is equal to the size
 * of the word. If the search hits a dead-end, the process starts over amd a new first letter
 * is grabbed from the hash.
 */
public class WordSolver {
    private static final Coordinate TOP_LEFT = new Coordinate(-1, -1);
    private static final Coordinate TOP_CENTER = new Coordinate(0, -1);
    private static final Coordinate TOP_RIGHT = new Coordinate(1, -1);
    private static final Coordinate LEFT = new Coordinate(-1, 0);
    private static final Coordinate RIGHT = new Coordinate(1, 0);
    private static final Coordinate BOTTOM_LEFT = new Coordinate(-1, 1);
    private static final Coordinate BOTTOM_CENTER = new Coordinate(0, 1);
    private static final Coordinate BOTTOM_RIGHT = new Coordinate(1, 1);

    private final ArrayList<String> words;

    private final Map<Character, ArrayList<Letter>> letterHash;

    private final int xSize;

    private final int ySize;

    public WordSolver(Map<Character, ArrayList<Letter>> letterHash, ArrayList<String> words, int xSize, int ySize) {
        this.words = words;
        this.xSize = xSize;
        this.ySize = ySize;
        this.letterHash = letterHash;
    }

    public List<Solution> solve() {
        ArrayList<Solution> results = new ArrayList<>();
        for (String word : words) {
            Solution topLeft = solveForWord(word, List.of(), TOP_LEFT, letterHash, 0);
            if (topLeft != null) {
                results.add(topLeft);
            }
            Solution topCenter = solveForWord(word, List.of(), TOP_CENTER, letterHash, 0);
            if (topCenter != null) {
                results.add(topCenter);
            }
            Solution topRight = solveForWord(word, List.of(), TOP_RIGHT, letterHash, 0);
            if (topRight != null) {
                results.add(topRight);
            }
            Solution left = solveForWord(word, List.of(), LEFT, letterHash, 0);
            if (left != null) {
                results.add(left);
            }
            Solution right = solveForWord(word, List.of(), RIGHT, letterHash, 0);
            if (right != null) {
                results.add(right);
            }
            Solution bottomLeft = solveForWord(word, List.of(), BOTTOM_LEFT, letterHash, 0);
            if (bottomLeft != null) {
                results.add(bottomLeft);
            }
            Solution bottomCenter = solveForWord(word, List.of(), BOTTOM_CENTER, letterHash, 0);
            if (bottomCenter != null) {
                results.add(bottomCenter);
            }
            Solution bottomRight = solveForWord(word, List.of(), BOTTOM_RIGHT, letterHash, 0);
            if (bottomRight != null) {
                results.add(bottomRight);
            }
        }
        return results;
    }

    private Solution solveForWord(String word, List<Letter> validLetters, Coordinate direction, Map<Character, ArrayList<Letter>> letterHash, int searchIndex) {
        if (searchIndex == word.length()) {
            final Letter firstLetter = validLetters.get(0);
            final Letter lastLetter = validLetters.get(validLetters.size() - 1);
            final Coordinate startCoord = new Coordinate(firstLetter.getXCoord(), firstLetter.getYCoord());
            final Coordinate endCoord = new Coordinate(lastLetter.getXCoord(), lastLetter.getYCoord());
            return new Solution(word, startCoord, endCoord);
        } else if (searchIndex == 0) {
            char letter = word.charAt(searchIndex);
            ArrayList<Letter> lettersInGrid = letterHash.get(letter);
            ArrayList<Solution> results = new ArrayList<>();
            for (Letter l : lettersInGrid) {
                List<Letter> foundLetters = new ArrayList<>();
                foundLetters.add(l);

                Solution solution = solveForWord(word, foundLetters, direction, letterHash, searchIndex + 1);
                if (solution != null) {
                    results.add(solution);
                }
            }

            if (results.size() > 0) {
                return results.get(0);
            }
            return null;
        } else {
            Letter lastLetter = validLetters.get(searchIndex - 1);
            char letter = word.charAt(searchIndex);
            ArrayList<Letter> lettersInGrid = letterHash.get(letter);
            ArrayList<Solution> results = new ArrayList<>();
            for (Letter l : lettersInGrid) {
                if (lastLetter.isAdjacent(l, direction.x, direction.y)) {
                    List<Letter> foundLetters = new ArrayList<>(validLetters);
                    foundLetters.add(l);
                    Solution solution = solveForWord(word, foundLetters, direction, letterHash, searchIndex + 1);
                    if (solution != null) {
                        results.add(solution);
                    }
                }
            }

            if (results.size() > 0) {
                return results.get(0);
            }
            return null;
        }
    }
}
