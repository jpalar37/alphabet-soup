/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlphabetSoup;

import java.util.Objects;

/**
 *
 * @author Jeremy Palardy
 *
 * This class is the letter class. It holds the char of the letter and the coordinates of said letter.
 * The equals method compares a letter object with that of another letter object.
 */
public class Letter {

    private int xCoord;

    private int yCoord;

    private char letter;

    public Letter(char letter, int xCoord, int yCoord) {

        this.letter = letter;

        this.xCoord = xCoord;

        this.yCoord = yCoord;

    }

    public char getLetter() {

        return this.letter;
    }

    public int getXCoord() {

        return this.xCoord;

    }

    public int getYCoord() {

        return this.yCoord;

    }

    public boolean isAdjacent(Letter that, int xOffset, int yOffset) {
        return that.yCoord == this.yCoord + yOffset && that.xCoord == this.xCoord + xOffset;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.xCoord;
        hash = 79 * hash + this.yCoord;
        hash = 79 * hash + Objects.hashCode(this.letter);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Letter other = (Letter) obj;
        if (this.xCoord != other.xCoord) {
            return false;
        }
        if (this.yCoord != other.yCoord) {
            return false;
        }
        if (!Objects.equals(this.letter, other.letter)) {
            return false;
        }
        return true;
    }

}
