/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlphabetSoup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author Jeremy Palardy
 *
 * This class is the main driver for the project. It reads in the file, and then delegates the problem solving to the WordSolver class.
 */
public class AlphabetSoup {

    private static ArrayList<String> solutions = new ArrayList<String>();

    private static int xMax = 0;

    private static int yMax = 0;

    private static int count = 0;
    
    public static void main(String[] args) throws FileNotFoundException, IOException {

        //Read in the input file
        File inputFile = new File("input.txt");

        Scanner scan = new Scanner(inputFile);

        String gridParams = scan.nextLine();

        String[] gridSize = gridParams.split("x");

        Map<Character, ArrayList<Letter>> gridHash = new HashMap<>();

        if (gridSize.length == 2) {

            xMax = Integer.parseInt(gridSize[0]);

            yMax = Integer.parseInt(gridSize[1]);
        }

        for( int y = 0; y < xMax; y++ ) {

            String[] line = scan.nextLine().split(" ");
            
            for (int x = 0; x < yMax; x++) {
                char key = line[x].charAt(0);
                
                Letter letter = new Letter(key, x, y);
                
                ArrayList<Letter> existingLetters = gridHash.get(letter.getLetter());
                if (existingLetters == null) {
                    existingLetters = new ArrayList<>();
                    gridHash.put(letter.getLetter(), existingLetters);
                }
                existingLetters.add(letter);
            }
        }
        
        while (scan.hasNextLine()) {
            solutions.add(scan.nextLine());
        }

        WordSolver solver = new WordSolver(gridHash, solutions, xMax, yMax);
        List<Solution> solutions = solver.solve();

        solutions.forEach(solution -> System.out.println(solution.toString()));
    }
}
