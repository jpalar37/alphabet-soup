/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlphabetSoup;

import java.util.Objects;

/**
 *
 * @author Jeremy Palardy
 *
 * This class provides the solution the problem. It contains the word, the starting coordinate and the end coordinate of the word.
 * The toString method returns the human-readable solution to the problem.
 */
public class Solution {
    public final String word;
    public final Coordinate startCoord;
    public final Coordinate endCoord;

    public Solution(String word, Coordinate startCoord, Coordinate endCoord) {
        this.word = word;
        Objects.requireNonNull(startCoord, "Start Coordinate must not be null.");
        this.startCoord = startCoord;
        Objects.requireNonNull(endCoord, "End Coordinate must not be null.");
        this.endCoord = endCoord;
    }

    @Override
    public String toString() {
        return this.word
                + " "
                + this.startCoord.toString()
                + " "
                + this.endCoord.toString();
    }
}
